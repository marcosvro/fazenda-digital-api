<p align="center">
  <img src="/assets/go.png" width="150">
</p>

<h2 align="center">Farm Helper Golang API</h2>
<p align="center">A farm Golang API to help employees register animals, carry out collection weighing and categorize animals.</p>

<h2>This API implements concepts such as</h2>
<ul>
  <li>Rest API</li>
  <li>Filtered lists</li>
  <li>MVC pattern</li>
  <li>Migrations</li>
  <li>MySQL DB</li>
  <li>Object Relational Mapper</li>
  <li>Attribute-based access control</li>
</ul>

<h2>API Documentation</h2>
[Open API Documentation published with Postman](https://documenter.getpostman.com/view/21043391/2s8YzMXQYX)

