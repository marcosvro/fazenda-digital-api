FROM golang

ADD . /go/src/fazenda-api

WORKDIR src/fazenda-api

RUN go mod download

RUN go install

ENTRYPOINT /go/bin/fazenda-api

EXPOSE 3000