package scripts

import (
	"fmt"
	"log"

	"gopkg.in/guregu/null.v3"

	"gitlab.com/marcosvro/fazenda-api/src/config"

	"gitlab.com/marcosvro/fazenda-api/src/api/repository"

	"gitlab.com/marcosvro/fazenda-api/src/api/models"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
)

// LoadSuperUser carrega as roles principais do sistema
func LoadSuperUser() {
	fmt.Println("Iniciando carga do super usuário..")
	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}
	db = db.Begin()
	defer db.Close()

	userRepo := repository.NewRepositoyCRUD(db, models.User{}, models.User{}.TableName(), models.Acesso{})
	repoAuth := repository.NewRepositoryAuth(db)

	user := models.User{}
	err = db.Model(models.User{}).Where("email = ?", config.SUPER_USER_EMAIL).Scan(&user).Error
	if err == nil || config.SUPER_USER_EMAIL == "" {
		fmt.Println("Carga finalizada, super usuário já existe!")
		return
	}

	fmt.Println("Criando super usuário..")
	// Objeto Role "Admin" com todas as permissões
	superUser := models.User{
		Nome:  null.StringFrom("Admin"),
		Email: config.SUPER_USER_EMAIL,
		Senha: config.SUPER_USER_PASSWORD,
	}

	// Salva Admin role no banco
	err = userRepo.Save(&superUser)
	if err != nil {
		fmt.Println("Carga finalizada com erros!")
		db.Rollback()
		return
	}
	fmt.Println("Usuário criado!")

	fmt.Println("Criando acesso 'Admin' para o super usuário..")

	// coleta role Admin
	adminRole := models.Role{}
	err = db.Model(&models.Role{}).Where("nome = ?", "Admin").Scan(&adminRole).Error
	if err != nil {
		fmt.Println(err)
		fmt.Println("Carga finalizada com erros!")
		db.Rollback()
		return
	}

	// Cria acesso no banco
	acesso := models.Acesso{
		UserID: superUser.ID,
		RoleID: adminRole.ID,
	}
	acesso, err = repoAuth.SaveAcesso(acesso)
	if err != nil {
		fmt.Println("Carga finalizada com erros!")
		db.Rollback()
		return
	}
	fmt.Println("Acesso criado!")

	db.Commit()
	fmt.Println("Carga finalizada com sucesso!")
}
