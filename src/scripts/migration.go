package scripts

import (
	"fmt"
	"log"

	"gitlab.com/marcosvro/fazenda-api/src/api/models"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
)

// MakeMigration realiza a criação de todas as tabelas do banco de dados
func MakeMigration() {
	fmt.Println("Iniciando migração")
	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	// remove Pesagem foreing keys
	if (db.HasTable(&models.Pesagem{})) {
		err = db.Model(&models.Pesagem{}).RemoveForeignKey("fazenda_id", "fazenda(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// remove PesagemIndividual foreing keys
	if (db.HasTable(&models.PesagemIndividual{})) {
		err = db.Model(&models.PesagemIndividual{}).RemoveForeignKey("fazenda_id", "fazenda(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.PesagemIndividual{}).RemoveForeignKey("pesagem_id", "pesagem(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.PesagemIndividual{}).RemoveForeignKey("vaca_id", "animal(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// remove Lote foreing keys
	if (db.HasTable(&models.Lote{})) {
		err = db.Model(&models.Lote{}).RemoveForeignKey("fazenda_id", "fazenda(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// remove Animal foreign keys
	if (db.HasTable(&models.Animal{})) {
		err = db.Model(&models.Animal{}).RemoveForeignKey("pai_id", "animal(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.Animal{}).RemoveForeignKey("mae_id", "animal(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.Animal{}).RemoveForeignKey("fazenda_id", "fazenda(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.Animal{}).RemoveForeignKey("lote_id", "lote(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// remove Acesso foreign keys
	if (db.HasTable(&models.Acesso{})) {
		err = db.Model(&models.Acesso{}).RemoveForeignKey("user_id", "user(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.Acesso{}).RemoveForeignKey("role_id", "role(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.Acesso{}).RemoveForeignKey("fazenda_id", "fazenda(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// remove TokenAcesso foreign keys
	if (db.HasTable(&models.TokenAcesso{})) {
		err = db.Model(&models.TokenAcesso{}).RemoveForeignKey("role_id", "role(id)").Error
		if err != nil {
			log.Fatal(err)
		}
		err = db.Model(&models.TokenAcesso{}).RemoveForeignKey("fazenda_id", "fazenda(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// remove Permissao foreign keys
	if (db.HasTable(&models.Permissao{})) {
		err = db.Model(&models.Permissao{}).RemoveForeignKey("role_id", "role(id)").Error
		if err != nil {
			log.Fatal(err)
		}
	}

	// Drop all tables
	err = db.DropTableIfExists(
		&models.TokenAcesso{},
		&models.Acesso{},
		&models.Permissao{},
		&models.PesagemIndividual{},
		&models.Animal{},
		&models.Lote{},
		&models.Pesagem{},
		&models.Fazenda{},
		&models.User{},
		&models.Role{},
	).Error
	if err != nil {
		log.Fatal(err)
	}

	// Create all tables
	err = db.AutoMigrate(
		&models.TokenAcesso{},
		&models.Acesso{},
		&models.Permissao{},
		&models.PesagemIndividual{},
		&models.Animal{},
		&models.Lote{},
		&models.Pesagem{},
		&models.Fazenda{},
		&models.User{},
		&models.Role{},
	).Error
	if err != nil {
		log.Fatal(err)
	}

	// Create foreign keys
	// pesagem FKs
	err = db.Model(&models.Pesagem{}).AddForeignKey("fazenda_id", "fazenda(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	// lote FKs
	err = db.Model(&models.Lote{}).AddForeignKey("fazenda_id", "fazenda(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	// animal FKs
	err = db.Model(&models.Animal{}).AddForeignKey("pai_id", "animal(id)", "SET NULL", "SET NULL").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.Animal{}).AddForeignKey("mae_id", "animal(id)", "SET NULL", "SET NULL").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.Animal{}).AddForeignKey("fazenda_id", "fazenda(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.Animal{}).AddForeignKey("lote_id", "lote(id)", "SET NULL", "SET NULL").Error
	if err != nil {
		log.Fatal(err)
	}
	// pesagem_individual FKs
	err = db.Model(&models.PesagemIndividual{}).AddForeignKey("fazenda_id", "fazenda(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.PesagemIndividual{}).AddForeignKey("pesagem_id", "pesagem(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.PesagemIndividual{}).AddForeignKey("vaca_id", "animal(id)", "SET NULL", "SET NULL").Error
	if err != nil {
		log.Fatal(err)
	}
	// acesso FKs
	err = db.Model(&models.Acesso{}).AddForeignKey("user_id", "user(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.Acesso{}).AddForeignKey("role_id", "role(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.Acesso{}).AddForeignKey("fazenda_id", "fazenda(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	// token_acesso FKs
	err = db.Model(&models.TokenAcesso{}).AddForeignKey("role_id", "role(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	err = db.Model(&models.TokenAcesso{}).AddForeignKey("fazenda_id", "fazenda(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
	// permissao FKs
	err = db.Model(&models.Permissao{}).AddForeignKey("role_id", "role(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatal(err)
	}
}
