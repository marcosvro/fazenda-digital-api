package scripts

import (
	"fmt"
	"log"

	"gitlab.com/marcosvro/fazenda-api/src/api/repository"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"

	"gitlab.com/marcosvro/fazenda-api/src/api/models"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
)

// LoadMainRoles carrega as roles principais do sistema
func LoadMainRoles() {
	fmt.Println("Iniciando carga das roles..")
	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}
	db = db.Begin()
	defer db.Close()

	repoRole := repository.NewRepositoyCRUD(db, models.Role{}, models.Role{}.TableName(), models.Acesso{})
	repoPermission := repository.NewRepositoyCRUD(db, models.Permissao{}, models.Permissao{}.TableName(), models.Acesso{})

	role := models.Role{}
	err = db.Model(&models.Role{}).Where("nome = ?", "Admin").Scan(&role).Error
	if err == nil || !role.ID.IsZero() {
		fmt.Println("Carga finalizada com erros, role já existe no banco!")
		return
	}

	fmt.Println("Criando Role 'Admin'..")
	// Objeto Role "Admin" com todas as permissões
	adminRole := models.Role{
		Nome:       "Admin",
		Permissoes: security.ALL_PERMISSIONS,
	}

	// Salva Admin role no banco
	err = repoRole.Save(&adminRole)
	if err != nil {
		db.Rollback()
		return
	}

	// Persiste permissions da role
	for _, permissionKeyWord := range adminRole.Permissoes {
		permission := models.Permissao{
			KeyWord: permissionKeyWord,
			RoleID:  adminRole.ID,
		}
		err = repoPermission.Save(&permission)
		if err != nil {
			db.Rollback()
			fmt.Println("Carga finalizada com erros!")
			return
		}
	}
	fmt.Println("Role criada!")

	db.Commit()
	fmt.Println("Carga finalizada com sucesso!")
}
