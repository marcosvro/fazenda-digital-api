package api

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/config"

	"gitlab.com/marcosvro/fazenda-api/src/api/routes"
)

// Run carrega as variaveis de ambiente e expõe a API na porta informada
func Run() {
	fmt.Printf("\nListening [::]:%d\n", config.PORT)
	listen(config.PORT)
}

// listen inicia a API na porta informada
func listen(port int) {
	r := routes.New()
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), r))
}
