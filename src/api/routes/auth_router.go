package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"
)

var authRoutes = []Route{
	Route{
		URI:                 "/auth/signin",
		Method:              http.MethodPost,
		Handler:             controllers.SignIn,
		isAuthenticated:     false,
		requiredPermissions: []string{},
	},
	Route{
		URI:                 "/auth/signup",
		Method:              http.MethodPost,
		Handler:             controllers.SignUp,
		isAuthenticated:     false,
		requiredPermissions: []string{},
	},
	Route{
		URI:                 "/auth/generateInviteToken",
		Method:              http.MethodPost,
		Handler:             controllers.CreateInviteToken,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_INVITE_TOKEN_CREATE},
	},
	Route{
		URI:                 "/auth/accessToken",
		Method:              http.MethodPost,
		Handler:             controllers.GetFazendaAccess,
		isAuthenticated:     false,
		requiredPermissions: []string{},
	},
}
