package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
)

var fazendaRoutes = []Route{
	Route{
		URI:                 "/fazendas",
		Method:              http.MethodPost,
		Handler:             controllers.CreateFazenda,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_FAZENDA_CREATE},
	},
	Route{
		URI:                 "/fazendas",
		Method:              http.MethodGet,
		Handler:             controllers.ListFazendas,
		isAuthenticated:     false,
		requiredPermissions: []string{},
	},
	Route{
		URI:                 "/fazendas/{id}",
		Method:              http.MethodGet,
		Handler:             controllers.GetFazenda,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_FAZENDA_READ},
	},
	Route{
		URI:                 "/fazendas/{id}",
		Method:              http.MethodPut,
		Handler:             controllers.UpdateFazenda,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_FAZENDA_WRITE},
	},
	Route{
		URI:                 "/fazendas/{id}",
		Method:              http.MethodDelete,
		Handler:             controllers.DeleteFazenda,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_FAZENDA_DELETE},
	},
}
