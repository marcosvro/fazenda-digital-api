package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
)

var statusRoutes = []Route{
	Route{
		URI:                 "/",
		Method:              http.MethodGet,
		Handler:             controllers.CheckAPI,
		isAuthenticated:     false,
		requiredPermissions: []string{},
	},
}
