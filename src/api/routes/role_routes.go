package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
)

var roleRoutes = []Route{
	Route{
		URI:                 "/roles",
		Method:              http.MethodPost,
		Handler:             controllers.CreateRole,
		isAuthenticated:     true,
		requiredPermissions: []string{},
	},
	Route{
		URI:                 "/roles",
		Method:              http.MethodGet,
		Handler:             controllers.ListRoles,
		isAuthenticated:     true,
		requiredPermissions: []string{},
	},
	Route{
		URI:                 "/roles/{id}",
		Method:              http.MethodGet,
		Handler:             controllers.GetRole,
		isAuthenticated:     true,
		requiredPermissions: []string{},
	},
	Route{
		URI:                 "/roles/{id}",
		Method:              http.MethodDelete,
		Handler:             controllers.DeleteRole,
		isAuthenticated:     true,
		requiredPermissions: []string{},
	},
}
