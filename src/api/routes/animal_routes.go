package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
)

var animalRoutes = []Route{
	{
		URI:                 "/animais",
		Method:              http.MethodPost,
		Handler:             controllers.CreateAnimal,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_ANIMAL_CREATE},
	},
	{
		URI:                 "/animais",
		Method:              http.MethodGet,
		Handler:             controllers.ListAnimais,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_ANIMAL_READ},
	},
	{
		URI:                 "/animais/{id}",
		Method:              http.MethodGet,
		Handler:             controllers.GetAnimal,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_ANIMAL_READ},
	},
	{
		URI:                 "/animais/{id}",
		Method:              http.MethodPut,
		Handler:             controllers.UpdateAnimal,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_ANIMAL_WRITE},
	},
	{
		URI:                 "/animais/{id}",
		Method:              http.MethodDelete,
		Handler:             controllers.DeleteAnimal,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_ANIMAL_DELETE},
	},
}
