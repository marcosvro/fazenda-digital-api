package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"

	"github.com/gorilla/mux"
)

// Route é o tipo dos objetos criados nos arquivos _routes.go
type Route struct {
	URI                 string
	Method              string
	Handler             func(http.ResponseWriter, *http.Request)
	requiredPermissions []string
	isAuthenticated     bool
}

// LoadAllRoutes Carrega todas as suas rotas nesta função
func LoadAllRoutes() []Route {
	routes := []Route{}
	routes = append(routes, statusRoutes...)
	routes = append(routes, fazendaRoutes...)
	routes = append(routes, loteRoutes...)
	routes = append(routes, animalRoutes...)
	routes = append(routes, authRoutes...)
	routes = append(routes, roleRoutes...)
	routes = append(routes, pesagemRoutes...)
	return routes
}

// SetupRoutes expõe todas as rotas carregadas pela função LoadAllRoutes
func SetupRoutes(r *mux.Router) *mux.Router {
	for _, route := range LoadAllRoutes() {
		if route.isAuthenticated {
			if len(route.requiredPermissions) > 0 {
				r.HandleFunc(
					route.URI,
					middlewares.SetRouteMiddlewareRequireAuthetication(
						middlewares.SetRouteMiddlewareAuthorization(route.requiredPermissions, route.Handler),
					),
				).Methods(route.Method)
				continue
			}
			r.HandleFunc(
				route.URI,
				middlewares.SetRouteMiddlewareRequireAuthetication(route.Handler),
			).Methods(route.Method)
			continue
		}
		r.HandleFunc(route.URI, route.Handler).Methods(route.Method)
	}
	return r
}

// SetPreRouterMiddlewares ativa middlewares que rodam antes da execução dos controladores
func SetPreRouterMiddlewares(r *mux.Router) *mux.Router {
	// Seta formato da resposta para JSON
	r.Use(middlewares.SetMiddlewareJSONHeader)
	// Pega parâmetros de paginação e coloca no contexto da requisição
	r.Use(middlewares.SetMiddlewarePagination)
	// Extrai conteúdo do access token para o context
	r.Use(middlewares.SetMiddlewareAuthetication)
	return r
}

// SetPostRouterMiddlewares ativa middlerwares que rodam depois da execução dos controladores
func SetPostRouterMiddlewares(r *mux.Router) *mux.Router {
	// Loga chamadas à API no formato <METHOD HOST:URI PROTO>
	r.Use(middlewares.SetMiddlewareRouterLogger)
	return r
}

// New cria e retorna um roteador  gorilla mux
func New() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	SetPreRouterMiddlewares(r)
	SetupRoutes(r)
	SetPostRouterMiddlewares(r)
	return r
}
