package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"
)

var pesagemRoutes = []Route{
	Route{
		URI:                 "/pesagens",
		Method:              http.MethodPost,
		Handler:             controllers.CreatePesagem,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_CREATE},
	},
	Route{
		URI:                 "/pesagens",
		Method:              http.MethodGet,
		Handler:             controllers.ListPesagem,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_READ},
	},
	Route{
		URI:                 "/pesagens/{id}",
		Method:              http.MethodDelete,
		Handler:             controllers.DeletePesagem,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_DELETE},
	},
	Route{
		URI:                 "/pesagens/{id}",
		Method:              http.MethodPut,
		Handler:             controllers.UpdatePesagem,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_WRITE},
	},
	Route{
		URI:                 "/pesagens/{id}/individual",
		Method:              http.MethodGet,
		Handler:             controllers.ListPesagemIndividual,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_INDIVIDUAL_READ},
	},
	Route{
		URI:                 "/pesagens/{id}/individual",
		Method:              http.MethodPost,
		Handler:             controllers.CreatePesagemIndividual,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_INDIVIDUAL_CREATE},
	},
	Route{
		URI:                 "/pesagens/last",
		Method:              http.MethodGet,
		Handler:             controllers.GetLastPesagem,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_PESAGEM_READ, security.PERMISSION_PESAGEM_INDIVIDUAL_READ},
	},
}
