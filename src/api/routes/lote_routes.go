package routes

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/controllers"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"
)

var loteRoutes = []Route{
	{
		URI:                 "/lotes",
		Method:              http.MethodPost,
		Handler:             controllers.CreateLote,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_LOTE_CREATE},
	},
	{
		URI:                 "/lotes",
		Method:              http.MethodGet,
		Handler:             controllers.ListLotes,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_LOTE_READ},
	},
	{
		URI:                 "/lotes/animais",
		Method:              http.MethodGet,
		Handler:             controllers.ListLoteAnimais,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_LOTE_READ, security.PERMISSION_ANIMAL_READ},
	},
	{
		URI:                 "/lotes/{id}",
		Method:              http.MethodGet,
		Handler:             controllers.GetLote,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_LOTE_READ},
	},
	{
		URI:                 "/lotes/{id}",
		Method:              http.MethodPut,
		Handler:             controllers.UpdateLote,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_LOTE_WRITE},
	},
	{
		URI:                 "/lotes/{id}",
		Method:              http.MethodDelete,
		Handler:             controllers.DeleteLote,
		isAuthenticated:     true,
		requiredPermissions: []string{security.PERMISSION_LOTE_DELETE},
	},
}
