package database

import (
	"gitlab.com/marcosvro/fazenda-api/src/config"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Connect inicia uma conexão com o banco de dados
func Connect() (*gorm.DB, error) {
	db, err := gorm.Open(config.DBDRIVER, config.DBURL)
	if config.DEBUG {
		db = db.Debug()
	}
	if err != nil {
		return nil, err
	}
	return db, nil
}
