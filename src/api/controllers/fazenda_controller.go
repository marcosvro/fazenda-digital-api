package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/repository"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"

	"github.com/gorilla/mux"

	"github.com/go-playground/validator"
)

// CreateFazenda cria uma fazenda e atribui o objeto criado à respota http
func CreateFazenda(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	fazenda := models.Fazenda{}
	err = json.Unmarshal(body, &fazenda)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(fazenda)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	// Persiste nova fazenda no banco
	repo := repository.NewRepositoyCRUD(db, models.Fazenda{}, models.Fazenda{}.TableName(), models.Acesso{})
	err = repo.Save(&fazenda)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusCreated, fazenda)
}

// ListFazendas atribui à resposta http a lista de todas as fazendas
func ListFazendas(w http.ResponseWriter, r *http.Request) {
	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Fazenda{}, db.NewScope(models.Fazenda{}).TableName(), models.Acesso{})

	// Pega lista páginada do BD
	pagination := r.Context().Value("pagination").(middlewares.Pagination)
	fazendas := []models.Fazenda{}
	err = repo.FindAll(&fazendas, pagination, map[string]interface{}{})
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusOK, fazendas)
}

// GetFazenda atribui à resposta http uma fazendo pelo seu id
func GetFazenda(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Fazenda{}, db.NewScope(models.Fazenda{}).TableName(), models.Acesso{})

	// Pega objeto do BD com o id informado
	fazenda := models.Fazenda{}
	err = repo.GetByID(&fazenda, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, fazenda)
}

// UpdateFazenda altera uma fazenda pelo id
func UpdateFazenda(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	fazenda := models.Fazenda{}
	err = json.Unmarshal(body, &fazenda)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para map[string]
	changes := map[string]interface{}{}
	fmt.Println(changes)
	err = json.Unmarshal(body, &changes)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	// v := validator.New()
	// err = v.Struct(fazenda)
	// if err != nil {
	// 	validationErrors := err.(validator.ValidationErrors)
	// 	responses.ERROR(w, http.StatusBadRequest, validationErrors)
	// 	return
	// }

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Fazenda{}, models.Fazenda{}.TableName(), models.Acesso{})

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.UpdateByID(&models.Fazenda{}, uint32(id), changes)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, rowsAffected)
}

// DeleteFazenda deleta uma fazenda pelo id
func DeleteFazenda(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Fazenda{}, models.Fazenda{}.TableName(), models.Acesso{})

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.DeleteByID(&models.Fazenda{}, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	w.Header().Set("Entity", fmt.Sprintf("%d", id))
	responses.JSON(w, http.StatusNoContent, rowsAffected)
}
