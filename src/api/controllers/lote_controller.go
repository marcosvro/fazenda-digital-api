package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/repository"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"

	"github.com/gorilla/mux"

	"github.com/go-playground/validator"
)

// CreateLote cria um Lote e atribui o objeto criado à respota http
func CreateLote(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	lote := models.Lote{}
	err = json.Unmarshal(body, &lote)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(lote)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	// Persiste nova lote no banco
	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Lote{}, models.Lote{}.TableName(), access)
	err = repo.Save(&lote)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusCreated, lote)
}

// ListLotes atribui à resposta http a lista páginada dos lotes
func ListLotes(w http.ResponseWriter, r *http.Request) {
	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Lote{}, models.Lote{}.TableName(), access)

	// Pega lista páginada do BD
	pagination := r.Context().Value("pagination").(middlewares.Pagination)
	lotes := []models.Lote{}
	err = repo.FindAll(&lotes, pagination, map[string]interface{}{})
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusOK, lotes)
}

// ListLoteAnimais atribui à resposta http a lista páginada dos lotes com os animais cadastrados em cada lote
func ListLoteAnimais(w http.ResponseWriter, r *http.Request) {
	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	//repo := repository.NewRepositoyCRUD(db, models.Lote{}, models.Lote{}.TableName(), access)
	repoLote := repository.NewRepositoryLote(db, access)

	// Pega lista páginada do BD
	err, lotes := repoLote.FindAll()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusOK, lotes)
}

// GetLote atribui à resposta http uma fazendo pelo seu id
func GetLote(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Lote{}, models.Lote{}.TableName(), access)

	// Pega objeto do BD com o id informado
	lote := models.Lote{}
	err = repo.GetByID(&lote, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, lote)
}

// UpdateLote altera uma fazenda pelo id
func UpdateLote(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	lote := models.Lote{}
	err = json.Unmarshal(body, &lote)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para map[string]
	changes := map[string]interface{}{}
	fmt.Println(changes)
	err = json.Unmarshal(body, &changes)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	// v := validator.New()
	// err = v.Struct(fazenda)
	// if err != nil {
	// 	validationErrors := err.(validator.ValidationErrors)
	// 	responses.ERROR(w, http.StatusBadRequest, validationErrors)
	// 	return
	// }

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Lote{}, models.Lote{}.TableName(), access)

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.UpdateByID(&models.Lote{}, uint32(id), changes)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, rowsAffected)
}

// DeleteLote deleta uma fazenda pelo id
func DeleteLote(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Lote{}, models.Lote{}.TableName(), access)

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.DeleteByID(&models.Lote{}, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	w.Header().Set("Entity", fmt.Sprintf("%d", id))
	responses.JSON(w, http.StatusNoContent, rowsAffected)
}
