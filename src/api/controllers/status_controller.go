package controllers

import (
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"
)

// CheckAPI retorna se a API está online
func CheckAPI(w http.ResponseWriter, r *http.Request) {

	resp := struct {
		Message string `json:"message"`
	}{
		Message: "Fazenda API is online!",
	}

	responses.JSON(w, http.StatusOK, resp)
}
