package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/repository"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"

	"github.com/gorilla/mux"

	"github.com/go-playground/validator"
)

func resolve(role models.Role, db *gorm.DB) (models.Role, error) {
	// resolve permissions of role
	repoPermission := repository.NewRepositoyCRUD(db, models.Permissao{}, models.Permissao{}.TableName(), models.Acesso{})
	permissions := []models.Permissao{}
	err := repoPermission.FindAll(&permissions, middlewares.Pagination{Limit: -1, Page: -1}, map[string]interface{}{"role_id": role.ID.Int64})
	if err != nil {
		return role, err
	}
	role.Permissoes = make([]string, len(permissions))
	for i, p := range permissions {
		role.Permissoes[i] = p.KeyWord
	}
	return role, nil
}

// CreateRole cria um Role e atribui o objeto criado à respota http
func CreateRole(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	role := models.Role{}
	err = json.Unmarshal(body, &role)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(role)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()
	db = db.Begin()
	repoRole := repository.NewRepositoyCRUD(db, models.Role{}, models.Role{}.TableName(), models.Acesso{})
	repoPermission := repository.NewRepositoyCRUD(db, models.Permissao{}, models.Permissao{}.TableName(), models.Acesso{})

	// Persiste nova role no banco
	err = repoRole.Save(&role)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	// Persiste permissions da role
	for _, permissionKeyWord := range role.Permissoes {
		permission := models.Permissao{
			KeyWord: permissionKeyWord,
			RoleID:  role.ID,
		}
		err = repoPermission.Save(&permission)
		if err != nil {
			db.Rollback()
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
	}

	db.Commit()
	responses.JSON(w, http.StatusCreated, role)
}

// ListRoles atribui à resposta http a lista páginada dos roles
func ListRoles(w http.ResponseWriter, r *http.Request) {
	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Role{}, models.Role{}.TableName(), models.Acesso{})

	// Pega lista páginada do BD
	pagination := r.Context().Value("pagination").(middlewares.Pagination)
	roles := []models.Role{}
	err = repo.FindAll(&roles, pagination, map[string]interface{}{})
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	// resolve permissions to all roles
	for i, r := range roles {
		roles[i], err = resolve(r, db)
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
	}

	responses.JSON(w, http.StatusOK, roles)
}

// GetRole atribui à resposta http uma fazendo pelo seu id
func GetRole(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Role{}, models.Role{}.TableName(), models.Acesso{})

	// Pega objeto do BD com o id informado
	role := models.Role{}
	err = repo.GetByID(&role, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// resolve permissions
	role, err = resolve(role, db)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, role)
}

// DeleteRole deleta uma fazenda pelo id
func DeleteRole(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repository.NewRepositoyCRUD(db, models.Role{}, models.Role{}.TableName(), models.Acesso{})

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.DeleteByID(&models.Role{}, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	w.Header().Set("Entity", fmt.Sprintf("%d", id))
	responses.JSON(w, http.StatusNoContent, rowsAffected)
}
