package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/repository"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"

	"github.com/gorilla/mux"

	"github.com/go-playground/validator"
)

// CreateAnimal cria um Animal e atribui o objeto criado à respota http
func CreateAnimal(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	animal := models.Animal{}
	err = json.Unmarshal(body, &animal)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(animal)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	// Persiste nova animal no banco
	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context access invalid"))
		return
	}
	repo := repository.NewRepositoyCRUD(db, models.Animal{}, models.Animal{}.TableName(), access)
	err = repo.Save(&animal)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusCreated, animal)
}

// ListAnimais atribui à resposta http a lista páginada dos lotes
func ListAnimais(w http.ResponseWriter, r *http.Request) {

	// Checa filtros
	search := map[string]interface{}{}
	if r.FormValue("buscar") != "" {
		if r.FormValue("buscarPor") != "" {
			var key = r.FormValue("buscarPor")
			if key != "nome" && key != "numero_chip" && key != "numero_brinco" {
				responses.ERROR(w, http.StatusBadRequest, errors.New("Invalid parameter 'buscarPor'"))
				return
			}
			search[r.FormValue("buscarPor")] = r.FormValue("buscar")
		} else {
			search["nome"] = r.FormValue("buscar")
		}
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context access invalid"))
		return
	}
	repo := repository.NewRepositoyCRUD(db, models.Animal{}, models.Animal{}.TableName(), access)

	// Pega lista páginada do BD
	pagination := r.Context().Value("pagination").(middlewares.Pagination)
	animais := []models.Animal{}

	err = repo.FindAll(&animais, pagination, search)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusOK, animais)
}

// GetAnimal atribui à resposta http uma fazendo pelo seu id
func GetAnimal(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context access invalid"))
		return
	}
	repo := repository.NewRepositoyCRUD(db, models.Animal{}, models.Animal{}.TableName(), access)

	// Pega objeto do BD com o id informado
	animal := models.Animal{}
	err = repo.GetByID(&animal, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, animal)
}

// UpdateAnimal altera uma fazenda pelo id
func UpdateAnimal(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	animal := models.Animal{}
	err = json.Unmarshal(body, &animal)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para map[string]
	changes := map[string]interface{}{}
	fmt.Println(changes)
	err = json.Unmarshal(body, &changes)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	// v := validator.New()
	// err = v.Struct(fazenda)
	// if err != nil {
	// 	validationErrors := err.(validator.ValidationErrors)
	// 	responses.ERROR(w, http.StatusBadRequest, validationErrors)
	// 	return
	// }

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context access invalid"))
		return
	}
	repo := repository.NewRepositoyCRUD(db, models.Animal{}, models.Animal{}.TableName(), access)

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.UpdateByID(&models.Animal{}, uint32(id), changes)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, rowsAffected)
}

// DeleteAnimal deleta uma fazenda pelo id
func DeleteAnimal(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context access invalid"))
		return
	}
	repo := repository.NewRepositoyCRUD(db, models.Animal{}, models.Animal{}.TableName(), access)

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.DeleteByID(&models.Animal{}, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	w.Header().Set("Entity", fmt.Sprintf("%d", id))
	responses.JSON(w, http.StatusNoContent, rowsAffected)
}
