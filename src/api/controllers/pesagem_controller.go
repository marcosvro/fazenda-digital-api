package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gopkg.in/guregu/null.v3"

	"gitlab.com/marcosvro/fazenda-api/src/api/database"
	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/repository"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"

	"github.com/gorilla/mux"

	"github.com/go-playground/validator"
)

// CreatePesagem cria um Pesagem e atribui o objeto criado à respota http
func CreatePesagem(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	pesagem := models.Pesagem{}
	err = json.Unmarshal(body, &pesagem)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(pesagem)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	// Persiste nova pesagem no banco
	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Pesagem{}, models.Pesagem{}.TableName(), access)
	err = repo.Save(&pesagem)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusCreated, pesagem)
}

// ListPesagem atribui à resposta http a lista páginada das pesagens
func ListPesagem(w http.ResponseWriter, r *http.Request) {
	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Pesagem{}, models.Pesagem{}.TableName(), access)

	// Pega lista páginada do BD
	pagination := r.Context().Value("pagination").(middlewares.Pagination)
	pesagens := []models.Pesagem{}
	err = repo.FindAll(&pesagens, pagination, map[string]interface{}{})
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusOK, pesagens)
}

// DeletePesagem deleta uma pesagem pelo id
func DeletePesagem(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Pesagem{}, models.Pesagem{}.TableName(), access)

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.DeleteByID(&models.Pesagem{}, uint32(id))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	w.Header().Set("Entity", fmt.Sprintf("%d", id))
	responses.JSON(w, http.StatusNoContent, rowsAffected)
}

// UpdatePesagem altera uma fazenda pelo id
func UpdatePesagem(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	pesagem := models.Pesagem{}
	err = json.Unmarshal(body, &pesagem)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para map[string]
	changes := map[string]interface{}{}
	fmt.Println(changes)
	err = json.Unmarshal(body, &changes)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.Pesagem{}, models.Pesagem{}.TableName(), access)

	// Pega objeto do BD com o id informado
	rowsAffected, err := repo.UpdateByID(&models.Pesagem{}, uint32(id), changes)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, rowsAffected)
}

// CreatePesagemIndividual cria um PesagemIndividual e atribui o objeto criado à respota http
func CreatePesagemIndividual(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	pesagemIndividual := models.PesagemIndividual{}
	err = json.Unmarshal(body, &pesagemIndividual)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	pesagemIndividual.PesagemID = null.NewInt(id, true)

	// Vailda JSON
	v := validator.New()
	err = v.Struct(pesagemIndividual)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Pega acesso
	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	// Inicia transação
	db = db.Begin()
	repoPesagemIndividual := repository.NewRepositoyCRUD(db, models.PesagemIndividual{}, models.PesagemIndividual{}.TableName(), access)
	repoPesagem := repository.NewRepositoyCRUD(db, models.Pesagem{}, models.Pesagem{}.TableName(), access)
	defer db.Close()

	// Persiste nova pesagem individual no banco
	err = repoPesagemIndividual.Save(&pesagemIndividual)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Pega pesagem
	pesagem := models.Pesagem{}
	err = repoPesagem.GetByID(&pesagem, uint32(pesagemIndividual.PesagemID.Int64))
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Atualiza peso total e quantidade de medições da pesagem
	changeCounts := map[string]interface{}{
		"peso_total":   pesagem.PesoTotal + pesagemIndividual.Peso,
		"qtd_pesagens": pesagem.QtdPesagens + 1,
	}
	_, err = repoPesagem.UpdateByID(&models.Pesagem{}, uint32(pesagem.ID.Int64), changeCounts)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Efetiva transação
	db.Commit()
	responses.JSON(w, http.StatusCreated, pesagemIndividual)
}

// ListPesagemIndividual atribui à resposta http a lista páginada das pesagens individuais por Pesagem
func ListPesagemIndividual(w http.ResponseWriter, r *http.Request) {
	//Pega id dos parâmetros da URL
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repo := repository.NewRepositoyCRUD(db, models.PesagemIndividual{}, models.PesagemIndividual{}.TableName(), access)

	// Pega lista páginada do BD
	pagination := r.Context().Value("pagination").(middlewares.Pagination)
	pesagensIndividuais := []models.PesagemIndividual{}
	err = repo.FindAll(&pesagensIndividuais, pagination, map[string]interface{}{"pesagem_id": id})
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusOK, pesagensIndividuais)
}

// GetLastPesagem atribui à resposta http a última pesagem registrada
func GetLastPesagem(w http.ResponseWriter, r *http.Request) {
	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context Access invalid"))
	}
	repoPesagens := repository.NewRepositoryPesagem(db, access)

	// Pega última pesagem
	pesagem, err := repoPesagens.GetLastPesagem()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	if pesagem.ID.IsZero() {
		responses.JSON(w, http.StatusNoContent, "{}")
	} else {
		responses.JSON(w, http.StatusOK, pesagem)
	}

}
