package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"gopkg.in/guregu/null.v3"

	"github.com/go-playground/validator"
	"gitlab.com/marcosvro/fazenda-api/src/api/database"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/repository"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"
)

type token struct {
	TokenAcesso null.String `json:"token_acesso"`
}

type fazendaAccessInput struct {
	RefreshToken string   `json:"refresh_token" validate:"required"`
	FazendaID    null.Int `json:"fazenda_id" validate:"required"`
}

type credentials struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

// GetFazendaAccess retorna um token de acesso JWT utilizado para acessas as funcionalidades da API
func GetFazendaAccess(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	refreshTokenInput := fazendaAccessInput{}
	err = json.Unmarshal(body, &refreshTokenInput)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(refreshTokenInput)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()
	repoAuth := repository.NewRepositoryAuth(db)

	// Gera access token
	accessToken, err := repoAuth.GetFazendaJWTAccessToken(refreshTokenInput.RefreshToken, refreshTokenInput.FazendaID)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusCreated, map[string]interface{}{"access_token": accessToken})
}

// SignIn checa as credenciais e responde com tokens jwt de acesso e refresh
func SignIn(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	credentials := credentials{}
	err = json.Unmarshal(body, &credentials)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(credentials)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()
	repoAuth := repository.NewRepositoryAuth(db)

	// Gera refresh token
	refreshToken, err := repoAuth.SignIn(credentials.Email, credentials.Password)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusCreated, map[string]interface{}{"refresh_token": refreshToken})
}

// SignUp cria é a função que responde a requisição de criação de novos usuários, podendo criar acessos junto aos mesmos dado um token de acesso
func SignUp(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(user)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()
	db = db.Begin()
	repoAuth := repository.NewRepositoryAuth(db)
	repoUserCRUD := repository.NewRepositoyCRUD(db, models.User{}, models.User{}.TableName(), models.Acesso{})
	repoTokenAcessoCRUD := repository.NewRepositoyCRUD(db, models.TokenAcesso{}, models.TokenAcesso{}.TableName(), models.Acesso{})

	// Checa token de acesso
	token := token{}
	tokenAcesso := models.TokenAcesso{}
	err = json.Unmarshal(body, &token)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if !token.TokenAcesso.IsZero() {
		// Caso tenha informado token de acesso verificar se token existe no banco
		tokenAcesso, err = repoAuth.GetTokenAcessoByToken(token.TokenAcesso.String)
		if err != nil {
			db.Rollback()
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("Access token invalid"))
			return
		}

		// Checa se token não foi expirado
		loc, err := time.LoadLocation("America/Sao_Paulo")
		if err != nil {
			db.Rollback()
			responses.ERROR(w, http.StatusInternalServerError, errors.New("Failed to get timezone location"))
			return
		}
		if time.Now().In(loc).After(tokenAcesso.DataExpiracaoToken.Time) {
			db.Rollback()
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("Access token has been expired"))
			return
		}
		// Checa se token já foi usado
		if tokenAcesso.Used {
			db.Rollback()
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("Access token has been used"))
			return
		}
	}

	// Persiste novo user no banco
	err = repoUserCRUD.Save(&user)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	if !tokenAcesso.ID.IsZero() {
		// Cria acesso com o token informado
		acesso := models.Acesso{}
		acesso.UserID = user.ID
		acesso.RoleID = tokenAcesso.RoleID
		acesso.FazendaID = tokenAcesso.FazendaID
		acesso.DataExpiracao = tokenAcesso.DataExpiracaoAcesso
		acesso, err = repoAuth.SaveAcesso(acesso)
		if err != nil {
			db.Rollback()
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}

		// Invalida token
		changeSetTokenUsed := map[string]interface{}{"used": true}
		_, err := repoTokenAcessoCRUD.UpdateByID(&models.TokenAcesso{}, uint32(tokenAcesso.ID.Int64), changeSetTokenUsed)
		if err != nil {
			db.Rollback()
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
	}

	// Efetiva transação
	db.Commit()

	responses.JSON(w, http.StatusCreated, user)
}

// CreateInviteToken cria um token para conceder acesso à algum usuário
func CreateInviteToken(w http.ResponseWriter, r *http.Request) {
	// Lê body da request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Converte body para JSON
	tokenAcesso := models.TokenAcesso{}
	err = json.Unmarshal(body, &tokenAcesso)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Vailda JSON
	v := validator.New()
	err = v.Struct(tokenAcesso)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		responses.ERROR(w, http.StatusBadRequest, validationErrors)
		return
	}

	// Inicia conexão com o banco de dados
	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	// Persiste nova tokenAcesso no banco
	access, ok := r.Context().Value("access").(models.Acesso)
	if !ok {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Context access invalid"))
		return
	}
	repo := repository.NewRepositoyCRUD(db, models.TokenAcesso{}, models.TokenAcesso{}.TableName(), access)
	err = repo.Save(&tokenAcesso)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, http.StatusCreated, tokenAcesso)
}
