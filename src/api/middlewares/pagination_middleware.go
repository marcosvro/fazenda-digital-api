package middlewares

import (
	"context"
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"
)

// DefaultLimitPage é o tamanho padão das páginas retornadas nas listas
const DefaultLimitPage = 10
const paginationContextKey = "pagination"

// Pagination é o objeto de paginação inserido na requisição por este middleware
type Pagination struct {
	Page  int
	Limit int
}

// SetMiddlewarePagination pega as informações de paginação usadas nos repositorys
func SetMiddlewarePagination(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var page, limit int = 1, DefaultLimitPage
		var err error

		if r.FormValue("pagina") != "" {
			page, err = strconv.Atoi(r.FormValue("pagina"))
			if err != nil {
				responses.ERROR(w, http.StatusBadRequest, errors.New("Invalid parameter page"))
				return
			}
		}

		if r.FormValue("limite") != "" {
			limit, err = strconv.Atoi(r.FormValue("limite"))
			if err != nil {
				responses.ERROR(w, http.StatusBadRequest, errors.New("Invalid parameter limit"))
				return
			}
		}

		if page <= 0 {
			page = 1
		}
		if limit <= 0 {
			limit = DefaultLimitPage
		}

		pagination := Pagination{
			Page:  page,
			Limit: limit,
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, paginationContextKey, pagination)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
