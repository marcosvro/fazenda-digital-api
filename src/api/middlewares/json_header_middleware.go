package middlewares

import (
	"net/http"
)

// SetMiddlewareJSONHeader informa aos clients que a resposta está no formato JSON
func SetMiddlewareJSONHeader(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
