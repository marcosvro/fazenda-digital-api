package middlewares

import (
	"log"
	"net/http"
)

// SetMiddlewareRouterLogger loga chamadas à API no formato <METHOD HOST:URI PROTO>
func SetMiddlewareRouterLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s%s %s", r.Method, r.Host, r.RequestURI, r.Proto)
		next.ServeHTTP(w, r)
	})
}
