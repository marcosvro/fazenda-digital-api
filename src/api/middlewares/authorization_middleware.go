package middlewares

import (
	"errors"
	"net/http"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"
)

// SetRouteMiddlewareAuthorization verifica se o usuário possui a permissão informada
func SetRouteMiddlewareAuthorization(requiredPermissions []string, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Pega permissões do contexto
		permissions, ok := r.Context().Value("permissions").([]string)
		if !ok {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Context permissions invalid"))
			return
		}

		// Checa permissão
		hasPermission := false
		for _, p := range permissions {
			for _, r := range requiredPermissions {
				if p == r {
					hasPermission = true
					break
				}
			}
			if hasPermission {
				break
			}
		}

		// Caso o token não tenha a permissão requerida
		if !hasPermission {
			responses.ERROR(w, http.StatusForbidden, errors.New("Access token has no permission to this service"))
			return
		}
		next(w, r)
	}
}
