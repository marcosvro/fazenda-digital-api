package middlewares

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/responses"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"
)

// SetMiddlewareAuthetication extrai o conteúdo do access token usado nas
func SetMiddlewareAuthetication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error

		// extrai token do header
		bearToken := r.Header.Get("Authorization")
		if bearToken == "" {
			next.ServeHTTP(w, r)
			return
		}
		strArr := strings.Split(bearToken, " ")
		if len(strArr) != 2 && strings.ToLower(strArr[0]) != "bearer" {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Invalid authentication token"))
			return
		}
		tokenString := strArr[1]

		// Pega conteúdo do token
		payload, err := security.GetPayloadFromTokenString(tokenString)
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, err)
			return
		}

		// convert access interface{} to models.Acesso
		jsonString, err := json.Marshal(payload["access"])
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Invalid access field of authentication token"))
			return
		}
		access := models.Acesso{}
		err = json.Unmarshal(jsonString, &access)
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Invalid access field of authentication token"))
			return
		}

		// convert permissions interface{} to []string
		jsonString, err = json.Marshal(payload["permissions"])
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Invalid permissions field of authentication token"))
			return
		}
		permissions := []string{}
		err = json.Unmarshal(jsonString, &permissions)
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Invalid permissions field of authentication token"))
			return
		}

		// insert authentication info to request context
		ctx := r.Context()
		ctx = context.WithValue(ctx, "permissions", permissions)
		ctx = context.WithValue(ctx, "access", access)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

// SetRouteMiddlewareRequireAuthetication bloqueia acesso ao endpoint quando nao processado um token de acesso
func SetRouteMiddlewareRequireAuthetication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Checa se o middleware de Authenticação identificou algum token enviado na requisição
		_, ok := r.Context().Value("access").(models.Acesso)
		if !ok {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Authentication required"))
			return
		}
		next(w, r)
	}
}
