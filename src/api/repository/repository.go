package repository

import (
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"gopkg.in/guregu/null.v3"

	"gitlab.com/marcosvro/fazenda-api/src/api/models"

	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/channels"

	"github.com/jinzhu/gorm"
)

type repositoyCRUD struct {
	db        *gorm.DB
	model     interface{}
	tableName string
	access    models.Acesso
}

// NewRepositoyCRUD cria uma instancia do repositório genérico com acesso as funções do CRUD
func NewRepositoyCRUD(db *gorm.DB, model interface{}, tableName string, access models.Acesso) *repositoyCRUD {
	if _, hasField := reflect.TypeOf(model).FieldByName("FazendaID"); !hasField {
		access.FazendaID = null.IntFromPtr(nil)
	}
	return &repositoyCRUD{db, model, tableName, access}
}

func (r *repositoyCRUD) Save(out interface{}) error {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Table(r.tableName).Create(out).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return nil
	}

	return err
}

func (r *repositoyCRUD) FindAll(out interface{}, pagination middlewares.Pagination, filter map[string]interface{}) error {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		db := r.db.Table(r.tableName)
		if !r.access.FazendaID.IsZero() {
			db = db.Where("fazenda_id = ?", r.access.FazendaID.Int64)
		}
		// apply filters
		for key, value := range filter {
			db = db.Where(fmt.Sprintf("%s = ?", key), value)
		}
		err = db.Offset(pagination.Limit * (pagination.Page - 1)).Limit(pagination.Limit).Scan(out).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return nil
	}

	return err
}

func (r *repositoyCRUD) GetByID(out interface{}, id uint32) error {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		db := r.db.Table(r.tableName).Where("id = ?", id)
		if !r.access.FazendaID.IsZero() {
			db = db.Where("fazenda_id = ?", r.access.FazendaID.Int64)
		}
		err = db.Take(out).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return nil
	}

	return err
}

func (r *repositoyCRUD) UpdateByID(model interface{}, id uint32, changes map[string]interface{}) (int64, error) {
	var err error
	done := make(chan bool)

	loc, err := time.LoadLocation("America/Sao_Paulo")
	if err != nil {
		return 0, err
	}
	changes["updated_at"] = time.Now().In(loc)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Table(r.tableName).Where("id = ?", id).Take(model).UpdateColumns(changes).Error
		ch <- true
	}(done)

	if channels.OK(done) {
		if err != nil {
			return 0, err
		}
		return 1, nil
	}

	return 0, err
}

func (r *repositoyCRUD) DeleteByID(model interface{}, id uint32) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)
	go func(ch chan<- bool) {
		defer close(ch)
		rs = r.db.Table(r.tableName).Where("id = ?", id).Take(model).Delete(model)
		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			return 0, rs.Error
		}
		return rs.RowsAffected, nil
	}

	return 0, rs.Error
}

// Utils

func structToMap(model interface{}) (map[string]interface{}, error) {
	p, err := json.Marshal(model) // encode to JSON
	if err != nil {
		return nil, err
	}
	var mapModel = map[string]interface{}{}
	json.Unmarshal(p, &mapModel)

	return mapModel, nil
}
