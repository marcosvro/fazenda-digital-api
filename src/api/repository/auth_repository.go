package repository

import (
	"encoding/json"
	"errors"

	"github.com/jinzhu/gorm"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/channels"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"
	"gopkg.in/guregu/null.v3"
)

type repositoryAuth struct {
	db *gorm.DB
}

// NewRepositoryAuth cria uma instancia do repositório com acesso as funções que acessam as tabelas de autenticação e autorização
func NewRepositoryAuth(db *gorm.DB) *repositoryAuth {
	return &repositoryAuth{db}
}

func (r *repositoryAuth) GetTokenAcessoByToken(token string) (models.TokenAcesso, error) {
	var err error
	tokenAcesso := models.TokenAcesso{}
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Model(&models.TokenAcesso{}).Where("token = ?", token).Take(&tokenAcesso).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return tokenAcesso, nil
	}

	return models.TokenAcesso{}, err
}

func (r *repositoryAuth) SaveAcesso(acesso models.Acesso) (models.Acesso, error) {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Model(&models.Acesso{}).Create(&acesso).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return acesso, nil
	}

	return models.Acesso{}, err
}

// SignIn verifica credenciais do usuário e gera um refresh token JWT quando válidas
func (r *repositoryAuth) SignIn(email string, password string) (string, error) {
	// Pega usuário do banco
	user := models.User{}
	err := r.db.Model(models.User{}).Where("email = ?", email).Scan(&user).Error
	if err != nil {
		return "", errors.New("Incorrect user or password")
	}

	// Verifica senha do usuário
	err = security.VerifyPassword(user.Senha, password)
	if err != nil {
		return "", errors.New("Incorrect user or password")
	}

	// Pega acessos do usuário
	acessos := []models.Acesso{}
	err = r.db.Model(&models.Acesso{}).Where("user_id = ?", user.ID.Int64).Find(&acessos).Error
	if err != nil {
		return "", err
	}

	// Gera payload
	payload := map[string]interface{}{
		"user_id": user.ID.Int64,
		"access":  acessos,
	}

	return security.CreateToken(payload)
}

// GetFazendaAccessToken retorna um JWT com as permissoens do usuário contido no refreshToken para a fazenda informada
func (r *repositoryAuth) GetFazendaJWTAccessToken(refreshToken string, fazendaID null.Int) (string, error) {
	// Pega conteúdo do token
	payload, err := security.GetPayloadFromTokenString(refreshToken)
	if err != nil {
		return "", err
	}

	// Converte interface{} para []models.Acesso
	acessos, err := mapArrayToAcessosArray(payload["access"])
	if err != nil {
		return "", err
		//return "", errors.New("User access invalid")
	}
	if len(acessos) == 0 {
		return "", errors.New("User has no access")
	}

	// Pega acesso referente à fazenda informado
	acesso, err := searchAccessByFazendaID(acessos, fazendaID)
	if err != nil {
		return "", err
	}

	// Pega permissões da role informada
	permissions := []models.Permissao{}
	err = r.db.Model(&models.Permissao{}).Where("role_id = ?", acesso.RoleID.Int64).Find(&permissions).Error
	if err != nil {
		return "", err
	}

	// Converte []models.Permissao para []string
	stringArrayPermissions := make([]string, len(permissions))
	for i, v := range permissions {
		stringArrayPermissions[i] = v.KeyWord
	}

	// Gera payload
	accessPayload := map[string]interface{}{
		"access":      acesso,
		"permissions": stringArrayPermissions,
	}

	return security.CreateToken(accessPayload)
}

func searchAccessByFazendaID(acessos []models.Acesso, fazendaID null.Int) (models.Acesso, error) {
	geralAccess := models.Acesso{}
	for _, access := range acessos {
		if access.FazendaID.IsZero() {
			geralAccess = access
		} else if access.FazendaID.Equal(fazendaID) {
			return access, nil
		}
	}
	if geralAccess.RoleID.IsZero() {
		return geralAccess, errors.New("User no has access to this fazenda_id")
	}
	return geralAccess, nil
}

func mapArrayToAcessosArray(mapArray interface{}) ([]models.Acesso, error) {
	jsonString, err := json.Marshal(mapArray)
	if err != nil {
		return []models.Acesso{}, err
	}

	acessosArray := []models.Acesso{}
	err = json.Unmarshal(jsonString, &acessosArray)
	if err != nil {
		return []models.Acesso{}, err
	}
	return acessosArray, nil
}
