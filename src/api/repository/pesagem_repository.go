package repository

import (
	"reflect"

	"github.com/jinzhu/gorm"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/channels"
	"gopkg.in/guregu/null.v3"
)

type repositoryPesagem struct {
	db     *gorm.DB
	access models.Acesso
}

// NewRepositoryPesagem cria uma instancia do repositório com acesso as funções que acessam as tabelas de autenticação e autorização
func NewRepositoryPesagem(db *gorm.DB, access models.Acesso) *repositoryPesagem {
	if _, hasField := reflect.TypeOf(models.Lote{}).FieldByName("FazendaID"); !hasField {
		access.FazendaID = null.IntFromPtr(nil)
	}
	return &repositoryPesagem{db, access}
}

// GetLastPesagem retorna a última pesagem finalizada contendo a lista de medições
func (r *repositoryPesagem) GetLastPesagem() (models.Pesagem, error) {
	var err error
	done := make(chan bool)

	var lastPesagem models.Pesagem

	go func(ch chan<- bool) {
		var pesagens []models.Pesagem
		defer close(ch)
		var db *gorm.DB = r.db.Model(models.Pesagem{})
		if !r.access.FazendaID.IsZero() {
			db = db.Where("fazenda_id = ?", r.access.FazendaID.Int64)
		}
		err = db.Where("finalizada = 1").Order("id desc").Limit(1).Find(&pesagens).Error
		if err != nil {
			ch <- false
			return
		}

		if len(pesagens) > 0 {
			lastPesagem = pesagens[0]
			db = r.db.Table(models.PesagemIndividual{}.TableName())
			if !r.access.FazendaID.IsZero() {
				db = db.Where("fazenda_id = ?", r.access.FazendaID.Int64)
			}
			var pesagensIndividuais = []models.PesagemIndividualMini{}
			err = db.Where("pesagem_id = ?", lastPesagem.ID).Select("vaca_id, peso").Find(&pesagensIndividuais).Error
			lastPesagem.Medicoes = pesagensIndividuais
		}

		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return lastPesagem, nil
	}

	return models.Pesagem{}, err
}
