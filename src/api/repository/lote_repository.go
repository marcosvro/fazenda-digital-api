package repository

import (
	"reflect"

	"github.com/jinzhu/gorm"
	"gitlab.com/marcosvro/fazenda-api/src/api/models"
	"gitlab.com/marcosvro/fazenda-api/src/api/utils/channels"
	"gopkg.in/guregu/null.v3"
)

type repositoryLote struct {
	db     *gorm.DB
	access models.Acesso
}

// NewRepositoryLote cria uma instancia do repositório com acesso as funções que acessam as tabelas de autenticação e autorização
func NewRepositoryLote(db *gorm.DB, access models.Acesso) *repositoryLote {
	if _, hasField := reflect.TypeOf(models.Lote{}).FieldByName("FazendaID"); !hasField {
		access.FazendaID = null.IntFromPtr(nil)
	}
	return &repositoryLote{db, access}
}

func (r *repositoryLote) FindAll() (error, []models.Lote) {
	var err error
	done := make(chan bool)

	var lotes []models.Lote
	var lotesAnimais []models.Lote

	go func(ch chan<- bool) {
		defer close(ch)
		var db *gorm.DB = r.db.Model(models.Lote{})
		if !r.access.FazendaID.IsZero() {
			db = db.Where("fazenda_id = ?", r.access.FazendaID.Int64)
		}
		err = db.Find(&lotes).Error
		if err != nil {
			ch <- false
			return
		}
		db = r.db.Model(models.Animal{})
		if !r.access.FazendaID.IsZero() {
			db = db.Where("fazenda_id = ?", r.access.FazendaID.Int64)
		}
		var animais []models.Animal
		err = db.Find(&animais).Error

		if err != nil {
			ch <- false
			return
		}

		for _, v := range lotes {
			v.Animais = filterAnimaisByLote(animais, v.ID)
			lotesAnimais = append(lotesAnimais, v)
		}

		noLote := models.Lote{}
		noLote.Animais = filterAnimaisByLote(animais, null.Int{})

		lotesAnimais = append(lotesAnimais, noLote)

		ch <- true
	}(done)

	if channels.OK(done) {
		return nil, lotesAnimais
	}

	return err, nil
}

func filterAnimaisByLote(array []models.Animal, loteID null.Int) []models.Animal {
	ret := []models.Animal{}
	for _, s := range array {
		if s.LoteID == loteID {
			ret = append(ret, s)
		}
	}
	return ret
}
