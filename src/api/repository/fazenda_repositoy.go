package repository

import (
	"time"

	"gitlab.com/marcosvro/fazenda-api/src/api/models"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/channels"

	"gitlab.com/marcosvro/fazenda-api/src/api/middlewares"

	"github.com/jinzhu/gorm"
)

type repositoyFazendaCRUD struct {
	db *gorm.DB
}

// NewRepositoyUserCRUD cria uma instancia do repositório com acesso as funções do CRUD
func NewRepositoyUserCRUD(db *gorm.DB) *repositoyFazendaCRUD {
	return &repositoyFazendaCRUD{db}
}

func (r *repositoyFazendaCRUD) Save(fazenda models.Fazenda) (models.Fazenda, error) {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Model(&models.Fazenda{}).Create(&fazenda).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return fazenda, nil
	}

	return models.Fazenda{}, err
}

func (r *repositoyFazendaCRUD) FindAll(pagination middlewares.Pagination) ([]models.Fazenda, error) {
	var err error
	done := make(chan bool)
	fazendas := []models.Fazenda{}

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Offset(pagination.Limit * (pagination.Page - 1)).Model(&models.Fazenda{}).Limit(pagination.Limit).Find(&fazendas).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return fazendas, nil
	}

	return []models.Fazenda{}, err
}

func (r *repositoyFazendaCRUD) GetByID(id uint32) (models.Fazenda, error) {
	var err error
	fazenda := models.Fazenda{}
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Model(&models.Fazenda{}).Where("id = ?", id).Take(&fazenda).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return fazenda, nil
	}

	return models.Fazenda{}, err
}

func (r *repositoyFazendaCRUD) UpdateByID(id uint32, fazenda models.Fazenda) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)
	go func(ch chan<- bool) {
		defer close(ch)
		rs = r.db.Model(&models.Fazenda{}).Where("id = ?", id).Take(&models.Fazenda{}).UpdateColumns(
			map[string]interface{}{
				"name":       fazenda.Name,
				"city":       fazenda.City,
				"area":       fazenda.Area,
				"updated_at": time.Now(),
			},
		)
		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			return 0, rs.Error
		}
		return rs.RowsAffected, nil
	}

	return 0, rs.Error
}

func (r *repositoyFazendaCRUD) DeleteByID(id uint32) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)
	go func(ch chan<- bool) {
		defer close(ch)
		rs = r.db.Model(&models.Fazenda{}).Where("id = ?", id).Take(&models.Fazenda{}).Delete(&models.Fazenda{})
		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			return 0, rs.Error
		}
		return rs.RowsAffected, nil
	}

	return 0, rs.Error
}
