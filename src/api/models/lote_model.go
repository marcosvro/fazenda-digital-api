package models

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

type Lote struct {
	ID        null.Int  `gorm:"primary_key;auto_increment" json:"id"`
	Nome      string    `gorm:"size:100;not null;unique" json:"nome" validate:"required"`
	Descricao string    `gorm:"size:200" json:"descricao"`
	Producao  bool      `gorm:"default:true" json:"producao"`
	Animais   []Animal  `json:"animais"`
	FazendaID null.Int  `gorm:"not null" json:"fazenda_id" validate:"required"`
	CreatedAt time.Time `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt time.Time `gorm:"type:timestamp" json:"updated_at"`
}

// LoteTableName é o nome da tabela Lote
const LoteTableName = "lote"

// TableName define the table name to gorm
func (Lote) TableName() string {
	return LoteTableName
}
