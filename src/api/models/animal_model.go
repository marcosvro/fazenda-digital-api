package models

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

type Animal struct {
	ID             null.Int    `gorm:"primary_key;auto_increment" json:"id"`
	Nome           null.String `gorm:"size:100" json:"nome"`
	DataNascimento null.Time   `json:"data_nascimento"`
	Sexo           string      `gorm:"size:1; not null" json:"sexo" validate:"oneof=F M; required"`
	PaiID          null.Int    `json:"pai_id"`
	MaeID          null.Int    `json:"mae_id"`
	Producao       bool        `gorm:"default:true" json:"producao"`
	FazendaID      null.Int    `gorm:"not null" json:"fazenda_id" validate:"required"`
	NumeroChip     string      `gorm:"size:50; not null; unique" json:"numero_chip" validate:"required"`
	NumeroBrinco   int         `gorm:"not null; unique" json:"numero_brinco" validate:"required"`
	LoteID         null.Int    `json:"lote_id"`
	Proprio        bool        `gorm:"default:true" json:"proprio"`
	Status         string      `gorm:"size:50; default:'ativo'" json:"status" validate:"oneof=ativo baixado"`
	DataBaixa      null.Time   `json:"data_baixa"`
	MotivoBaixa    null.String `gorm:"type:text" json:"motivo_baixa"`
	CreatedAt      time.Time   `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt      time.Time   `gorm:"type:timestamp" json:"updated_at"`
}

// AnimalTableName é o nome da tabela Animal
const AnimalTableName = "animal"

// TableName define the table name to gorm
func (Animal) TableName() string {
	return AnimalTableName
}
