package models

import (
	"gopkg.in/guregu/null.v3"
)

type Acesso struct {
	ID            null.Int  `gorm:"primary_key;auto_increment" json:"id"`
	UserID        null.Int  `gorm:"not null" json:"user_id"`
	FazendaID     null.Int  `json:"fazenda_id"`
	RoleID        null.Int  `gorm:"not null" json:"role_id"`
	DataExpiracao null.Time `json:"data_expiracao"`
}

// AcessoTableName é o nome da tabela Acesso
const AcessoTableName = "acesso"

// TableName define the table name to gorm
func (Acesso) TableName() string {
	return AcessoTableName
}
