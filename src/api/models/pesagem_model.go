package models

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

type Pesagem struct {
	ID          null.Int                `gorm:"primary_key;auto_increment" json:"id"`
	FazendaID   null.Int                `gorm:"not null" json:"fazenda_id" validate:"required"`
	Finalizada  bool                    `gorm:"default:false" json:"finalizada"`
	PesoTotal   float32                 `gorm:"default:0.0" json:"peso_total"`
	QtdPesagens int32                   `gorm:"default:0" json:"qtd_pesagens"`
	Medicoes    []PesagemIndividualMini `json:"medicoes"`
	CreatedAt   time.Time               `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt   time.Time               `gorm:"type:timestamp" json:"updated_at"`
}

// PesagemTableName é o nome da tabela Pesagem
const PesagemTableName = "pesagem"

// TableName define the table name to gorm
func (Pesagem) TableName() string {
	return PesagemTableName
}
