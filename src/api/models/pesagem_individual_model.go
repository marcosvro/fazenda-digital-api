package models

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

type PesagemIndividual struct {
	ID        null.Int  `gorm:"primary_key;auto_increment" json:"id"`
	PesagemID null.Int  `gorm:"not null" json:"pesagem_id" validate:"required"`
	FazendaID null.Int  `gorm:"not null" json:"fazenda_id" validate:"required"`
	VacaID    null.Int  `json:"vaca_id" validate:"required"`
	Peso      float32   `gorm:"not null" json:"peso" validate:"required"`
	CreatedAt time.Time `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt time.Time `gorm:"type:timestamp" json:"updated_at"`
}

// PesagemIndividualTableName é o nome da tabela PesagemIndividual
const PesagemIndividualTableName = "pesagem_individual"

// TableName define the table name to gorm
func (PesagemIndividual) TableName() string {
	return PesagemIndividualTableName
}

type PesagemIndividualMini struct {
	VacaID null.Int `json:"vaca_id"`
	Peso   float32  `json:"peso"`
}
