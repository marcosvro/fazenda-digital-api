package models

import (
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gopkg.in/guregu/null.v3"
)

type TokenAcesso struct {
	ID                  null.Int    `gorm:"primary_key;auto_increment" json:"id"`
	FazendaID           null.Int    `gorm:"not null" json:"fazenda_id" validate:"required"`
	RoleID              null.Int    `gorm:"not null" json:"role_id" validate:"required"`
	DataExpiracaoToken  null.Time   `json:"data_expiracao_token"`
	Token               null.String `gorm:"size:100; unique" json:"token"`
	DataExpiracaoAcesso null.Time   `json:"data_expiracao_acesso"`
	Used                bool        `gorm:"default:false" json:"used"`
	CreatedAt           time.Time   `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt           time.Time   `gorm:"type:timestamp" json:"updated_at"`
}

// TokenAcessoTableName é o nome da tabela TokenAcesso
const TokenAcessoTableName = "token_acesso"

// TableName define the table name to gorm
func (TokenAcesso) TableName() string {
	return TokenAcessoTableName
}

// BeforeCreate criar um token único e uma data de Expiração do token de 7 dias.
func (base *TokenAcesso) BeforeCreate(scope *gorm.Scope) error {
	// set token
	uuid := uuid.NewV4().String()
	err := scope.SetColumn("Token", uuid)
	if err != nil {
		return err
	}

	// set data_expiracao_token
	next7DaysDate := time.Now().AddDate(0, 0, 7)
	err = scope.SetColumn("DataExpiracaoToken", next7DaysDate)
	if err != nil {
		return err
	}

	return nil
}
