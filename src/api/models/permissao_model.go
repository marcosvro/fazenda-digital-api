package models

import (
	"gopkg.in/guregu/null.v3"
)

type Permissao struct {
	ID      null.Int `gorm:"primary_key;auto_increment" json:"id"`
	RoleID  null.Int `gorm:"not null" json:"role_id" validate:"required"`
	KeyWord string   `gorm:"size:100;not null" json:"key_word" validate:"required"`
}

// PermissaoTableName é o nome da tabela Permissao
const PermissaoTableName = "permissao"

// TableName define the table name to gorm
func (Permissao) TableName() string {
	return PermissaoTableName
}
