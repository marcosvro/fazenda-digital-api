package models

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

type Fazenda struct {
	ID        null.Int  `gorm:"primary_key;auto_increment" json:"id"`
	Name      string    `gorm:"size:100;not null;unique" json:"name" validate:"required"`
	City      string    `gorm:"size:100;not null" json:"city" validate:"required"`
	Area      uint32    `gorm:"not null" json:"area" validate:"required"`
	CreatedAt time.Time `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt time.Time `gorm:"type:timestamp" json:"updated_at"`
}

// FazendaTableName é o nome da tabela Fazenda
const FazendaTableName = "fazenda"

// TableName define the table name to gorm
func (Fazenda) TableName() string {
	return FazendaTableName
}
