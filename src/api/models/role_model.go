package models

import (
	"time"

	"gopkg.in/guregu/null.v3"
)

type Role struct {
	ID         null.Int  `gorm:"primary_key;auto_increment" json:"id"`
	Nome       string    `gorm:"size:100;unique" json:"nome" validate:"required"`
	Permissoes []string  `gorm:"-" json:"permissoes" validate:"required"`
	CreatedAt  time.Time `gorm:"type:timestamp;type:timestamp;default:current_timestamp()" json:"created_at"`
}

// RoleTableName é o nome da tabela Role
const RoleTableName = "role"

// TableName define the table name to gorm
func (Role) TableName() string {
	return RoleTableName
}
