package models

import (
	"time"

	"gopkg.in/guregu/null.v3"

	"gitlab.com/marcosvro/fazenda-api/src/api/utils/security"
)

type User struct {
	ID              null.Int    `gorm:"primary_key;auto_increment" json:"id"`
	Nome            null.String `gorm:"size:200" json:"nome" validate:"required"`
	Senha           string      `gorm:"size:100; not null" json:"senha" validate:"required"`
	Telefone        null.String `gorm:"size:20" json:"telefone"`
	Email           string      `gorm:"size:50; not null; unique" json:"email" validate:"required"`
	EmailVerificado bool        `gorm:"default:false" json:"email_verificado"`
	CreatedAt       time.Time   `gorm:"type:timestamp;default:current_timestamp()" json:"created_at"`
	UpdatedAt       time.Time   `gorm:"type:timestamp" json:"updated_at"`
}

// UserTableName é o nome da tabela User
const UserTableName = "user"

// TableName define the table name to gorm
func (User) TableName() string {
	return UserTableName
}

// BeforeSave é um override de uma função invocada um pouco antes do dado ser persistido no banco
func (u *User) BeforeSave() error {
	hashedPassword, err := security.Hash(u.Senha)
	if err != nil {
		return err
	}
	u.Senha = string(hashedPassword)
	return nil
}
