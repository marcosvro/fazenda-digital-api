package console

import (
	"encoding/json"
	"fmt"
	"log"
)

// JSONPretty printa um objeto json formatado
func JSONPretty(data interface{}) {
	b, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(string(b))
}
