package security

import "golang.org/x/crypto/bcrypt"

// Hash gera uma senha com o algoritmo de Hash do bycript
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// VerifyPassword checa se a password em textoplano é equivalente a hashedPassword gerada pelo bcrypt
func VerifyPassword(hashedPassword string, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}
