package security

import (
	"fmt"
	"time"

	"gitlab.com/marcosvro/fazenda-api/src/config"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"
)

type Claim struct {
	UserID string `json:"user_id"`
}

// CreateToken cria um token JWT para o usuário
func CreateToken(claims map[string]interface{}) (string, error) {
	claims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = claims
	token.Valid = true
	return token.SignedString([]byte(config.JWT_SECRET))
}

// GetPayloadFromTokenString retornar o conteúdo do token
func GetPayloadFromTokenString(tokenString string) (map[string]interface{}, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(config.JWT_SECRET), nil
	})
	if err != nil {
		return nil, err
	}
	return token.Claims, nil
}
