package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var (
	PORT                = 0
	JWT_SECRET          = ""
	DBDRIVER            = ""
	DBURL               = ""
	SUPER_USER_EMAIL    = ""
	SUPER_USER_PASSWORD = ""
	DEBUG               = false
)

// LoadVariables carrega as variaveis de ambiente
func LoadVariables(local bool) {
	var err error
	//carrega variaveis com dotenv
	if local {
		err = godotenv.Load()
		if err != nil {
			log.Fatal(err)
		}
	}

	// carrega porta
	PORT, err = strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		PORT = 3000
	}

	// carrega DB configs
	DBDRIVER = os.Getenv("DBDRIVER")
	DBURL = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=America%%2FSao_Paulo&time_zone=%%27%%2B13%%3A00%%27", os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

	// carrega segredo jwt da API
	JWT_SECRET = os.Getenv("JWT_SECRET")

	// carrega credenciais do super usuário
	SUPER_USER_EMAIL = os.Getenv("SUPER_USER_EMAIL")
	SUPER_USER_PASSWORD = os.Getenv("SUPER_USER_PASSWORD")
}
