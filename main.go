package main

import (
	"gitlab.com/marcosvro/fazenda-api/src/config"
	"gitlab.com/marcosvro/fazenda-api/src/scripts"

	"gitlab.com/marcosvro/fazenda-api/src/api"

	"github.com/spf13/pflag"
)

func main() {
	var migrate *bool = pflag.BoolP("migrate", "m", false, "Execute database migrations")
	var load *bool = pflag.BoolP("load", "l", false, "Execute load scripts")
	var debug *bool = pflag.BoolP("debug", "d", false, "Show all SQL query to DB")
	var local *bool = pflag.BoolP("local", "L", false, "Execute with local environmnet variables")
	pflag.Parse()

	config.LoadVariables(*local)
	if *debug {
		config.DEBUG = true
	}

	if *migrate {
		scripts.MakeMigration()
	} else {
		if *load {
			scripts.LoadMainRoles()
			scripts.LoadSuperUser()
		}
		api.Run()
	}
}
