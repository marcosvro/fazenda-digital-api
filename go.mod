module gitlab.com/marcosvro/fazenda-api

go 1.15

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.3.3
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	gopkg.in/dgrijalva/jwt-go.v2 v2.7.0
	gopkg.in/guregu/null.v3 v3.5.0
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.5
)
